const express = require('express')
const app = express()
const port = 5555

app.get('/', (req, res) => {
  res.send('Coucou Authentification!')
})

app.listen(port, () => {
  console.log(`Listening http://localhost:${port}`)
})